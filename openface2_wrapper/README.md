OpenFace2 Wrapper
=================

## Overview

This package is an OpenFace 2.0 ROS wrapper. It contains example code to detect faces and compute facial features using
OpenFace's API and models. The package also includes OpenFace, OpenCV and DLib as submodules in the `libraries` directory
for locally installing these libraries within the package. See this [README.md](../README.md) for installation instructions.

It is **highly recommended to provide rectified images** to the OpenFace nodes or launch files included in this wrapper
because OpenFace does not deal with image distortion and relies on intrinsic camera parameters when estimating head pose.

## Launch

One convenient launch file is provided to run most of the functionality of this wrapper:

```bash
roslaunch openface2_wrapper detect_faces_and_features.launch image_topic:=/camera/image_raw camera_info_topic:=/camera/camera_info
```

The launch file first runs the face detection node, and then runs another node to compute facial landmarks, head pose,
gaze, and action units. See the launch file for a complete list of parameters.

To display the detected face info on RViz you can launch with an optional parameter as follows:

```bash
roslaunch openface2_wrapper detect_faces_and_features.launch image_topic:=/camera/image_raw camera_info_topic:=/camera/camera_info rviz_visualization:=true
```

## Nodes

### 1. Face detection with Multi-Task Cascaded Convolutional Neural Network (MTCNN)

Run MTCNN on input images and output face detections 
as [opencv_app/FaceArrayStamped](http://docs.ros.org/kinetic/api/opencv_apps/html/msg/FaceArrayStamped.html).

Example usage with a compressed image topic:

```bash
rosrun openface2_wrapper mtcnn_face_detector _image_topic:=/camera/image_raw _image_transport:=compressed _verbose:=1 _visualize:=true
```

#### Published Topics

**faces** ([opencv_app/FaceArrayStamped](http://docs.ros.org/kinetic/api/opencv_apps/html/msg/FaceArrayStamped.html)): Output faces

#### Parameters

**~image_topic** (string, default: image_rect): Input image topic.

**~image_transport** (string, default: raw): Image transport for the image_topic.

**~verbose** (int, default: 0): Print information while processing images? (yes if verbose > 0)

**~visualize** (bool, default: false): Visualize detected faces with OpenCV while processing?

**~processing_image_width** (int, default: 0) Width of image for processing (if 0, process in its original size)


### 2. Facial landmarks, head pose, gaze estimation, and Action Units

Get features on face detections from input images. The `face_features_node`
subscribes to synchronized images and face detections (as output by
mtcnn_face_detector). The images and face detections must have the same stamp.

Example usage with a compressed image topic:

```bash
rosrun openface2_wrapper face_features_node  _image_topic:=/camera/image_raw _image_transport:=compressed _camera_info_topic:=/camera/camera_info /faces:=/camera/faces
```

As mentioned before, the node works best with rectified images since OpenFace 2
does not consider image distortion.

### 3. Projecting faces on to RViz

This optional node will take the recorded information and display the head position, landmark points, and gaze direction on RViz as markers. When run, it publishes marker arrays to two additional topics: /face_array, /point_array, and /eye_array

#### Published Topics

**face_features** (opencv2_wrapper/FaceFeaturesArray): Output features

#### Parameters

**~image_topic** (string, default: image_rect): Input image topic (ideally, rectified images).

**~image_transport** (string, default: raw): Image transport for the image_topic.

**~camera_info_topic** (string, default: camera_info): Topic with camera info.

**~verbose** (int, default: 0): Print information while processing images? (yes if verbose > 0)

**~visualize** (bool, default: false): Visualize detected faces with OpenCV while processing?

**~processing_image_width** (int, default: 0) Width of image for processing (if 0, process in its original size)
