// Wrapper for the OpenFace 2.0 MTCNN face detector
// Created by Marynel Vazquez on 7/7/19.

#ifndef OPENFACE2_ROS_WRAPPER_MTCNN_WRAPPER_H
#define OPENFACE2_ROS_WRAPPER_MTCNN_WRAPPER_H

#include "openface2_wrapper/openface2_pointer_types.h"
#include <opencv2/opencv.hpp>
#include <opencv_apps/FaceArrayStamped.h>

namespace openface2_wrapper {

    /**
     * Wrapper for the MTCNN face detection capabilities of OpenFace 2.0.
     * Can be used to detect faces from nodes or from standalone executables.
     */
    class MTCNNWrapper {
    public:

        FaceDetectorMTCNNPtr face_detector_mtcnn_ptr;           //!< Face detector
        std::string mtcnn_face_detector_location;               //!< Location of face detector

        /**
         * Default constructor
         */
        MTCNNWrapper();

        /**
         * Constructor
         * @param face_detector_location location of face detector
         */
        MTCNNWrapper(const std::string& face_detector_location);

        /**
         * Destructor
         */
        ~MTCNNWrapper();

        /**
         * Load face detector. Must be called before detecting faces with MTCNNWrapper::detect_faces().
         * @return true if the detector was loaded successfully; false otherwise
         */
        bool loadFaceDetector();

        /**
         * Detect faces in an OpenCV image
         * @param input_image input image
         * @param face_detections face detections
         * @param confidences confidences for the face detections
         * @param scale scale for the image while processing (>= 0, where 1.0 means original scale)
         * @return true if the image was successfully processed; false if the face detector is unset
         */
        bool detectFaces(const cv::Mat& input_image,
                         std::vector<cv::Rect_<float> >& face_detections,
                         std::vector<float>& confidences,
                         float scale = 1.0);

    };

}

#endif //OPENFACE2_ROS_WRAPPER_MTCNN_WRAPPER_H
