// Node that detects faces and facial landmarks using OpenFace 2.0.
// Created by Marynel Vazquez on 6/30/19.

#ifndef OPENFACE2_ROS_WRAPPER_FACE_LANDMARKS_H
#define OPENFACE2_ROS_WRAPPER_FACE_LANDMARKS_H

#include "openface2_wrapper/openface2_pointer_types.h"
#include "openface2_wrapper/face_features_wrapper.h"

#include <memory>
#include <ros/ros.h>
#include <image_transport/subscriber_filter.h>
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/synchronizer.h>
#include <opencv_apps/FaceArrayStamped.h>

#include <OpenFace/LandmarkCoreIncludes.h>
#include <OpenFace/Visualizer.h>


namespace openface2_wrapper {

    class FaceFeaturesNode {

    public:

        /** Policy for synchronizing images and faces */
        typedef message_filters::sync_policies::ExactTime< sensor_msgs::Image, opencv_apps::FaceArrayStamped > FFNodePolicy;
        /** Pointer to message synchronizer **/
        typedef std::shared_ptr< message_filters::Synchronizer< FFNodePolicy > > SynchronizerPtr;
        /** Pointer to synchronized subscriber of FaceArrayStamped messages */
        typedef std::shared_ptr< message_filters::Subscriber<opencv_apps::FaceArrayStamped> > MFSubscriberPtr;
        /** Pointer to synchronized subscriber of Image messages */
        typedef std::shared_ptr< image_transport::SubscriberFilter > SubscriberFilterPtr;

        /**
         * Constructor
         */
        FaceFeaturesNode();

        /**
         * Destructor
         */
        ~FaceFeaturesNode();

        /**
         * Image callback
         * @param image_msg image message
         * @param faces_msg faces message
         */
        void callback(const sensor_msgs::ImageConstPtr& image_msg,
                      const opencv_apps::FaceArrayStampedConstPtr& faces_msg);

    protected:

        // node stuff
        ros::NodeHandle nh_;                                    //!< Node handle
        int verbose;                                            //!< Verbose level (0 means quiet)
        bool au_detection;                                      //!< Perform AU detection?
        bool visualize;                                         //!< Run visualizer?
        int processing_image_width;                             //!< If 0, process at original size!

        // image stuff
        ImageTransportPtr it_ptr;                               //!< Image transport
        std::string transport;                                  //!< Transport
        SubscriberFilterPtr sub_image_ptr;                      //!< Image subscriber
        std::string image_topic;                                //!< Image topic

        // faces stuff
        MFSubscriberPtr sub_faces_ptr;                          //!< Faces topic
        SynchronizerPtr sync_ptr;                               //!< Image and Faces message synchronizer
        std::string face_detector_topic;                        //!<  Face detector topic, defaults to mtcnn_face_detector

        // camera info stuff
        ros::Subscriber sub_camera_info;                        //!< Camera info subscriber
        std::string camera_info_topic;                          //!< Camera info topic
        float fx;                                               //!< horizontal focal length from camera intrinsics
        float fy;                                               //!< vertical focal length from camera intrinsics
        float cx;                                               //!< cx from camera intrinsics
        float cy;                                               //!< cy from camera intrinsics

        // output stuff
        ros::Publisher pub_face_features;                       //!< Publisher for face landmarks

        // openface stuff
        FaceFeatureWrapper ff_wrapper;
        VisualizerPtr visualizer_ptr;                           //!< Visualizer


    };

}

#endif //OPENFACE2_ROS_WRAPPER_FACE_LANDMARKS_H
