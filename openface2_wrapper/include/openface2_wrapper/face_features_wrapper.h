//
// Created by marynelv on 7/7/19.
//

#ifndef OPENFACE2_ROS_WRAPPER_FACE_FEATURES_H
#define OPENFACE2_ROS_WRAPPER_FACE_FEATURES_H

#include "openface2_wrapper/openface2_pointer_types.h"

#include <opencv_apps/FaceArrayStamped.h>
#include <openface2_wrapper/FaceFeaturesArray.h>

#include <OpenFace/LandmarkCoreIncludes.h>
#include <OpenFace/Visualizer.h>

namespace openface2_wrapper {

    class FaceFeatureWrapper
    {
    public:

        typedef struct FFResult {
            bool success;                                               //!< successfully computed face features?
            cv::Mat_<float> detected_landmarks;                         //!< 2D model shape [x1,x2,...xn,y1,...yn]
            cv::Mat_<float> detected_landmarks_3D;                      //!< 3D model shape Nx3
            cv::Mat_<int> landmark_visibilities;                        //!< Currently non-self occluded landmarks
            cv::Vec6d pose_estimate;                                    //!< Head pose estimate
            cv::Point3f gaze_left;                                      //!< Gaze absolute (left eye)
            cv::Point3f gaze_right;                                     //!< Gaze absolute (right eye)
            cv::Vec2f gaze_angle;                                       //!< Gaze angle
            std::vector< std::pair<std::string, double> > current_au;   //!< Intensity of Action Units
        } FFResult;

        std::string au_model;                                   //!< Action Units model

        LandmarkDetector::FaceModelParameters det_parameters;   //!< Parameters for face and landmark detection
        CLNFPtr face_model_ptr;                                 //!< Facial landmarks model

        FaceAnalysis::FaceAnalyserParameters fa_parameters;     //!< Face analysis parameters (for AUs)
        FaceAnalyserPtr face_analyser_ptr;                      //!< Face analyser (for AUs)


        /**
         * Default constructor
         */
        FaceFeatureWrapper();

        /**
         * Constructor
         * @param landmarks_model path to landmarks model
         * @param au_model path to action unit model
         */
        FaceFeatureWrapper(const std::string& landmarks_model, const std::string& au_model);

        /**
         * Destructor
         */
        ~FaceFeatureWrapper();

        /**
        * Default initialization of OpenFace's FaceModelParameters for in the wild exec
        */
        void defaultInitFaceModelParams();

        /**
        * Default initialization of OpenFace's FaceAnalyser
        */
        void defaultInitFaceAnalyserParams();

        /**
         * Set landmarks model based on OpenFace's FaceModelParameters
         * @return true if the model was set properly; false otherwise
         */
        bool loadLandmarksModel();

        /**
         * Set Action Unit model
         * @return true if the model was set properly; false otherwise
         */
        bool loadAUModel();

        /**
         * Compute face features
         * @param input_image input image
         * @param face_detections input faces in the image (e.g., output by MTCNNWrapper::detectFaces())
         * @param fx fx from camera intrinsics
         * @param fy fy from camera intrinsics
         * @param cx cx from camera intrinsics
         * @param cy cy from camera intrinsics
         * @param result_vec output vector with face features
         * @param scale scale for the image while processing (>= 0, where 1.0 means original scale)
         * @param au_detection detect facial units?
         * @param visualizer_ptr pointer to visualizer in case we want to show the result with OpenFace's rendering code
         * @return true if the features were computed; false otherwise
         */
        bool computeFaceFeatures(const cv::Mat& input_image,
                                 const std::vector<cv::Rect_<float> >& face_detections,
                                 float fx, float fy, float cx, float cy,
                                 std::vector< FFResult >& result_vec,
                                 float scale = 0.0, bool au_detection = true,
                                 VisualizerPtr visualizer_ptr = nullptr);



    protected:

        void init();

    };

}

#endif //OPENFACE2_ROS_WRAPPER_FACE_FEATURES_H
