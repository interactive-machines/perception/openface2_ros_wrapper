// Useful typedefs
// Created by Marynel Vazquez on 7/1/19.

#ifndef OPENFACE2_ROS_WRAPPER_OPENFACE2_POINTER_TYPES_H
#define OPENFACE2_ROS_WRAPPER_OPENFACE2_POINTER_TYPES_H

#include <memory>
#include <image_transport/image_transport.h>
#include <OpenFace/LandmarkCoreIncludes.h>
#include <OpenFace/FaceAnalyser.h>
#include <OpenFace/Visualizer.h>

namespace openface2_wrapper {

    typedef std::shared_ptr <LandmarkDetector::CLNF> CLNFPtr;
    typedef std::shared_ptr <LandmarkDetector::FaceDetectorMTCNN> FaceDetectorMTCNNPtr;
    typedef std::shared_ptr <FaceAnalysis::FaceAnalyser> FaceAnalyserPtr;
    typedef std::shared_ptr <Utilities::Visualizer> VisualizerPtr;

    typedef std::shared_ptr< image_transport::ImageTransport > ImageTransportPtr;
}


#endif //OPENFACE2_ROS_WRAPPER_OPENFACE2_POINTER_TYPES_H
