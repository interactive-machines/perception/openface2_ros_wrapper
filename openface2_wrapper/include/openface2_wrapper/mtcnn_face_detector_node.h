// MTCNN face detector node. Relies on the MTCNNWrapper.
// Created by Marynel Vazquez on 7/1/19.

#ifndef OPENFACE2_ROS_WRAPPER_MTCNN_FACE_DETECTOR_NODE_H
#define OPENFACE2_ROS_WRAPPER_MTCNN_FACE_DETECTOR_NODE_H

#include <memory>
#include <ros/ros.h>
#include <OpenFace/LandmarkCoreIncludes.h>
#include <OpenFace/Visualizer.h>

#include "openface2_wrapper/openface2_pointer_types.h"
#include "openface2_wrapper/mtcnn_wrapper.h"

namespace openface2_wrapper {

    class MTCnnNode {

    public:

        /**
         * Constructor
         */
        MTCnnNode();

        /**
         * Destructor
         */
        ~MTCnnNode();

        /**
         * Image callback
         * @param msg image message
         */
        void imageCallback(const sensor_msgs::ImageConstPtr& msg);

    protected:

        ros::NodeHandle nh_;                                    //!< Node handle
        int verbose;                                            //!< Verbose level (0 means quiet)
        bool visualize;                                         //!< Run visualizer?
        int processing_image_width;                             //!< If 0, process at original size!

        ImageTransportPtr it_ptr;                               //!< Image transport
        std::string transport;                                  //!< Transport
        image_transport::Subscriber sub_image;                  //!< Image subscriber
        std::string image_topic;                                //!< Image topic

        ros::Publisher pub_faces;                               //!< Publisher

        MTCNNWrapper mtcnn_wrapper;                             //!< Wrapper for the OpenFace 2.0 MTCNN face detector


    };


}



#endif //OPENFACE2_ROS_WRAPPER_MTCNN_FACE_DETECTOR_NODE_H
