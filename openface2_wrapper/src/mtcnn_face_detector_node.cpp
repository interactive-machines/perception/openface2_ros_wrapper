// MTCNN face detector node
// Created by Marynel Vazquez on 7/1/19.

#include "openface2_wrapper/mtcnn_face_detector_node.h"

#include <opencv2/highgui/highgui.hpp>

#include <ros/package.h>
#include <sensor_msgs/CameraInfo.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv_apps/Face.h>
#include <opencv_apps/FaceArrayStamped.h>

#include <opencv2/imgproc/imgproc.hpp>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include <sstream>

using namespace openface2_wrapper;


// Constructor
MTCnnNode::
MTCnnNode()
{
    // private params
    ros::NodeHandle pnh = ros::NodeHandle("~");
    pnh.param<std::string>("image_transport", transport, std::string("raw"));
    pnh.param<std::string>("image_topic", image_topic, std::string("image_rect"));
    pnh.param<int>("processing_image_width", processing_image_width, 0);
    pnh.param<int>("verbose", verbose, 0);
    pnh.param<bool>("visualize", visualize, false);

    if (verbose)
        ROS_INFO_STREAM("Image topic: " << image_topic);

    // load face detector
    if (!mtcnn_wrapper.loadFaceDetector()) {
        ROS_ERROR_STREAM("Failed to load MTCNN face detector from " << mtcnn_wrapper.mtcnn_face_detector_location << ".");
        exit(1);
    }

    // set visualizer
    if (visualize) {
        namedWindow( "Faces", cv::WINDOW_AUTOSIZE ); // Create a window for display.
    }

    // set connections
    pub_faces = pnh.advertise<opencv_apps::FaceArrayStamped>("faces", 20);

    it_ptr = std::make_shared< image_transport::ImageTransport >(image_transport::ImageTransport(pnh));
    image_transport::TransportHints hints(transport, ros::TransportHints(), pnh);
    sub_image = it_ptr->subscribe(image_topic, 1, &MTCnnNode::imageCallback, this, hints);
}


// Destructor
MTCnnNode::
~MTCnnNode()
{
    if (visualize)
        cv::destroyWindow("Faces");
}


// Image callback
void
MTCnnNode::
imageCallback(const sensor_msgs::ImageConstPtr& image_msg)
{
    // convert image message to OpenCV mat
    cv_bridge::CvImagePtr cv_ptr;
    try {
        cv_ptr = cv_bridge::toCvCopy(image_msg, sensor_msgs::image_encodings::BGR8);
    } catch (cv_bridge::Exception& e) {
        ROS_ERROR("Failed to convert image message. Got cv_bridge exception: %s", e.what());
        return;
    }

    // compute scale for processing
    float scale = 1.0;
    if (processing_image_width > 0) {
        scale = float(processing_image_width) / float(cv_ptr->image.cols);
        if (verbose) ROS_INFO_STREAM("Scaling image by " << scale);
    }

    // detect faces
    std::vector<cv::Rect_<float> > face_detections;
    std::vector<float> confidences;
    if (!mtcnn_wrapper.detectFaces(cv_ptr->image, face_detections, confidences, scale))
    {
        ROS_ERROR("Failed to detect faces. Is the face detector setup?");
        return;
    }

    // publish output
    opencv_apps::FaceArrayStamped msg;
    msg.header = image_msg->header;

    for (size_t i = 0; i < face_detections.size(); ++i)
    {
        cv::Point_<float> tl = face_detections.at(i).tl();
        cv::Point_<float> br = face_detections.at(i).br();

        opencv_apps::Face face_msg;
        face_msg.face.x = tl.x;
        face_msg.face.y = tl.y;
        face_msg.face.width = br.x - tl.x;
        face_msg.face.height = br.y - tl.y;
        face_msg.confidence = confidences.at(i);

        msg.faces.push_back(face_msg);
    }

    pub_faces.publish(msg);

    // visualize result
    if (visualize) {
        for (size_t i = 0; i < msg.faces.size(); ++i)
        {
            cv::Point_<float> tl, br;

            tl.x = msg.faces.at(i).face.x;
            tl.y = msg.faces.at(i).face.y;
            br.x = tl.x + msg.faces.at(i).face.width;
            br.y = tl.y + msg.faces.at(i).face.height;

            cv::rectangle(cv_ptr->image, tl, br, cv::Scalar(0, 0, 255), 2, 8);
        }
        cv::imshow("Faces", cv_ptr->image);
        cv::waitKey(1);
    }

}

// main
int main(int argc, char **argv) {
    ros::init(argc, argv, "mtcnn_face_detector", ros::init_options::AnonymousName);
    MTCnnNode node = MTCnnNode();
    ros::spin();
}
