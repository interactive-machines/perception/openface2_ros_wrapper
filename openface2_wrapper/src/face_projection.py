#!/usr/bin/env python

import rospy
from openface2_wrapper.msg import FaceFeaturesArray
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Point
import tf.transformations
import numpy as np

# Hyper-parameters
SLEEP_RATE = 50


class FaceProjectionNode(object):
    """
    Visualizes face recognition information as markers on RViz
    """

    def __init__(self):
        # Everything besides pubs, subs, services
        self.face_array = MarkerArray()
        self.point_array = MarkerArray()
        self.eye_array = MarkerArray()

        # Publishers
        self.face_array_pub = rospy.Publisher('/face_array', MarkerArray, queue_size=1)
        self.point_array_pub = rospy.Publisher('/point_array', MarkerArray, queue_size=1)
        self.eye_array_pub = rospy.Publisher('/eye_array', MarkerArray, queue_size=1)

        # Subscribers
        rospy.Subscriber('/face_features_node/face_features', FaceFeaturesArray, self.face_features_cb)

    def face_create(self, index, msg):
        # Creates a marker to represent a face and its positional information
        marker_object = Marker()
        marker_object.header = msg.header
        marker_object.ns = 'face_visualization'
        marker_object.id = index
        marker_object.type = Marker.ARROW
        marker_object.action = Marker.ADD

        marker_pos = Point()
        marker_pos.x = msg.features[index].head_pose.position.x
        marker_pos.y = msg.features[index].head_pose.position.y
        marker_pos.z = msg.features[index].head_pose.position.z
        marker_object.pose.position = marker_pos

        # we rotate the orientation here because by convention arrows point towards +x
        q1 = [msg.features[index].head_pose.orientation.x,
             msg.features[index].head_pose.orientation.y,
             msg.features[index].head_pose.orientation.z,
             msg.features[index].head_pose.orientation.w]
        q2 = tf.transformations.quaternion_about_axis(np.pi * 0.5, (0, 1, 0))
        q = tf.transformations.quaternion_multiply(q2, q1)

        marker_object.pose.orientation.x = q[0]
        marker_object.pose.orientation.y = q[1]
        marker_object.pose.orientation.z = q[2]
        marker_object.pose.orientation.w = q[3]

        marker_object.scale.x = 0.04
        marker_object.scale.y = 0.02
        marker_object.scale.z = 0.02

        marker_object.color.r = 1.0
        marker_object.color.g = 0.0
        marker_object.color.b = 0.0
        marker_object.color.a = 1.0

        # If a face is lost for more than half a second, its marker will disappear
        marker_object.lifetime = rospy.Duration(.5)

        self.face_array.markers.append(marker_object)

    def point_create(self, index, msg, point_num):
        # Creates a marker to represent a landmark point and its positional information
        marker_object = Marker()
        marker_object.header = msg.header
        marker_object.ns = 'point_visualization'
        marker_object.id = int(str(index * 10) + str(point_num))
        marker_object.type = Marker.SPHERE
        marker_object.action = Marker.ADD

        marker_pos = Point()
        marker_pos.x = msg.features[index].landmarks_3D[point_num].x
        marker_pos.y = msg.features[index].landmarks_3D[point_num].y
        marker_pos.z = msg.features[index].landmarks_3D[point_num].z
        marker_object.pose.position = marker_pos

        marker_object.scale.x = 0.018
        marker_object.scale.y = 0.018
        marker_object.scale.z = 0.018

        marker_object.color.r = 1.0
        marker_object.color.g = 1.0
        marker_object.color.b = 0.0
        marker_object.color.a = 1.0

        # If a landmark is lost for more than half a second, its marker will disappear
        marker_object.lifetime = rospy.Duration(.5)

        self.point_array.markers.append(marker_object)

    def eye_create(self, index, msg, center, id):
        # Creates a maker to represent an eye and its positional information
        # id distinguishes the two eyes, 0 = left, 1 = right
        marker_object = Marker()
        marker_object.header = msg.header
        marker_object.ns = 'eye_visualization'
        marker_object.id = int(str(index)+ str(id))
        marker_object.type = Marker.ARROW
        marker_object.action = Marker.ADD

        marker_object.pose.position = center

        # Transform gaze from vector to quaternion
        if id == 0:
            pitch = np.arcsin(-1 * msg.features[index].gaze_left.z)
            yaw = np.arctan2(msg.features[index].gaze_left.y, msg.features[index].gaze_left.x)
        else:
            pitch = np.arcsin(-1 * msg.features[index].gaze_right.z)
            yaw = np.arctan2(msg.features[index].gaze_right.y, msg.features[index].gaze_right.x)

        q = tf.transformations.quaternion_from_euler(0, pitch, yaw, axes='sxyz')

        marker_object.pose.orientation.x = q[0]
        marker_object.pose.orientation.y = q[1]
        marker_object.pose.orientation.z = q[2]
        marker_object.pose.orientation.w = q[3]

        marker_object.scale.x = 0.1
        marker_object.scale.y = 0.01
        marker_object.scale.z = 0.01

        marker_object.color.r = 0.0
        marker_object.color.g = 1.0
        marker_object.color.b = 0.0
        marker_object.color.a = 1.0

        # If an eye is lost for more than half a second, its marker will disappear
        marker_object.lifetime = rospy.Duration(0.5)

        self.eye_array.markers.append(marker_object)

    def face_features_cb(self, msg):
        # Clears the marker array
        self.face_array.markers *= 0
        self.point_array.markers *= 0
        self.eye_array.markers *= 0
        print(msg)

        # For each face detected a new marker is created and added to the array
        if msg.features:
            for index in range(len(msg.features)):
                if msg.features[index].success:
                    self.face_create(index=index, msg=msg)

                    # The position of the eyes are calculated as an average of specific landmark points
                    left_eye_x = 0
                    left_eye_y = 0
                    left_eye_z = 0
                    right_eye_x = 0
                    right_eye_y = 0
                    right_eye_z = 0

                    for point_num in range(68):
                        self.point_create(index=index, msg=msg, point_num=point_num)

                    # Determine the center of each eye through averaging
                    for num in range(36, 42):
                        left_eye_x += msg.features[index].landmarks_3D[num].x
                        left_eye_y += msg.features[index].landmarks_3D[num].y
                        left_eye_z += msg.features[index].landmarks_3D[num].z
                    for num in range(42, 48):
                        right_eye_x += msg.features[index].landmarks_3D[num].x
                        right_eye_y += msg.features[index].landmarks_3D[num].y
                        right_eye_z += msg.features[index].landmarks_3D[num].z
                    left_center = Point()
                    left_center.x = left_eye_x / 6
                    left_center.y = left_eye_y / 6
                    left_center.z = left_eye_z / 6
                    right_center = Point()
                    right_center.x = right_eye_x / 6
                    right_center.y = right_eye_y / 6
                    right_center.z = right_eye_z / 6

                    self.eye_create(index=index, msg=msg, center=left_center, id=0)
                    self.eye_create(index=index, msg=msg, center=right_center, id=1)

        self.face_array_pub.publish(self.face_array)
        self.point_array_pub.publish(self.point_array)
        self.eye_array_pub.publish(self.eye_array)


def main():
    rospy.init_node("face_projection_node")

    face_projection_node = FaceProjectionNode()

    rate = rospy.Rate(SLEEP_RATE)
    # This while loop is what keeps the node from dying
    while not rospy.is_shutdown():
        rate.sleep()


if __name__ == '__main__':
    main()
