// Node that detects facial landmarks and estimates action units using OpenFace 2.0.
// Created by Marynel Vazquez on 6/30/19.

#include "openface2_wrapper/face_features_node.h"
#include <openface2_wrapper/FaceFeatures.h>
#include <openface2_wrapper/FaceFeaturesArray.h>

#include <OpenFace/VisualizationUtils.h>
#include <OpenFace/GazeEstimation.h>

#include <ros/package.h>
#include <sensor_msgs/CameraInfo.h>
#include <std_msgs/Int32.h>
#include <opencv_apps/Point2D.h>
#include <cv_bridge/cv_bridge.h>
#include <tf/transform_datatypes.h>

#include <opencv2/imgproc/imgproc.hpp>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include <sstream>

using namespace openface2_wrapper;


void euler2RotationMatrix(float roll, float pitch, float yaw, tf::Matrix3x3& out_mat)
{
		cv::Matx33f rotation_matrix;

		float s1 = sin(roll);
		float s2 = sin(pitch);
		float s3 = sin(yaw);

		float c1 = cos(roll);
		float c2 = cos(pitch);
		float c3 = cos(yaw);

        out_mat = tf::Matrix3x3(c2 * c3,
                            -c2 *s3,
                            s2,
                            c1 * s3 + c3 * s1 * s2,
                            c1 * c3 - s1 * s2 * s3,
                            -c2 * s1,
                            s1 * s3 - c1 * c3 * s2,
                            c3 * s1 + c1 * s2 * s3,
                            c1 * c2);

}



// Constructor
FaceFeaturesNode::FaceFeaturesNode()
{
    // private node params
    ros::NodeHandle pnh = ros::NodeHandle("~");
    pnh.param<std::string>("image_transport", transport, std::string("raw"));
    pnh.param<std::string>("image_topic", image_topic, std::string("image_rect"));
    pnh.param<std::string>("camera_info_topic", camera_info_topic, std::string("camera_info"));
    pnh.param<std::string>("face_detector_topic", face_detector_topic, std::string("/mtcnn_face_detector/faces"));
    pnh.param<int>("processing_image_width", processing_image_width, 0);
    pnh.param<int>("verbose", verbose, 1);
    pnh.param<bool>("au_detection", au_detection, true);
    pnh.param<bool>("visualize", visualize, true);
    pnh.param<float>("sigma", ff_wrapper.det_parameters.sigma, ff_wrapper.det_parameters.sigma);
    pnh.param<float>("reg_factor", ff_wrapper.det_parameters.reg_factor, ff_wrapper.det_parameters.reg_factor);
    pnh.param<float>("weight_factor", ff_wrapper.det_parameters.weight_factor, ff_wrapper.det_parameters.weight_factor);
    pnh.param<int>("num_optimisation_iteration", ff_wrapper.det_parameters.num_optimisation_iteration, ff_wrapper.det_parameters.num_optimisation_iteration);
    pnh.param<std::string>("landmarks_model", ff_wrapper.det_parameters.model_location, ff_wrapper.det_parameters.model_location);
    pnh.param<std::string>("au_model", ff_wrapper.au_model, ff_wrapper.au_model);

    // set landmarks model based on model_location
    if (!ff_wrapper.loadLandmarksModel()) {
        ROS_ERROR_STREAM("Failed to load landmarks model from " << ff_wrapper.det_parameters.model_location);
        exit(1);
    }

    // set au model based on au_location
    // see https://github.com/TadasBaltrusaitis/OpenFace/wiki/Action-Units for more info on AU
    if (!ff_wrapper.loadAUModel()) {
        ROS_ERROR_STREAM("Failed to load Action Units model from " << ff_wrapper.au_model);
        exit(1);
    }

    ROS_INFO_STREAM("Setting up visualizer");
    // set visualizer
    if (visualize) {
        Utilities::Visualizer visualizer(true, false, false, au_detection);
        visualizer_ptr = std::make_shared< Utilities::Visualizer >(visualizer);
    }

    ROS_INFO_STREAM("Setting up camera, start bag playback to populate camera info topic now");
    // get camera intrinsics
    boost::shared_ptr< const sensor_msgs::CameraInfo > camera_info_ptr =
            ros::topic::waitForMessage< sensor_msgs::CameraInfo > (camera_info_topic, ros::Duration(120));
    for (int r = 120; camera_info_ptr == NULL; r--) {
      // 1 sec sleep
      ros::Duration(1).sleep();
      if (r <= 0) {
        ROS_ERROR_STREAM("Failed to get camera info from " << camera_info_topic);
        exit(1);
      }
    }

    fx = camera_info_ptr->K[0];
    fy = camera_info_ptr->K[4];
    cx = camera_info_ptr->K[2];
    cy = camera_info_ptr->K[5];

    ROS_INFO_STREAM("Setting up pub/sub");
    // set publishers
    pub_face_features = pnh.advertise<openface2_wrapper::FaceFeaturesArray>("face_features", 10);

    // set subscribers
    image_transport::TransportHints hints(transport, ros::TransportHints(), pnh);
    it_ptr = std::make_shared< image_transport::ImageTransport >(image_transport::ImageTransport(pnh));
    sub_image_ptr = SubscriberFilterPtr(new image_transport::SubscriberFilter(*it_ptr, image_topic, 10, hints));

    sub_faces_ptr = MFSubscriberPtr(new message_filters::Subscriber<opencv_apps::FaceArrayStamped>(pnh, face_detector_topic, 10));

    sync_ptr = SynchronizerPtr(new message_filters::Synchronizer< FFNodePolicy >(FFNodePolicy(10), *sub_image_ptr, *sub_faces_ptr));
    sync_ptr->registerCallback( boost::bind(&FaceFeaturesNode::callback, this, _1, _2) );
    ROS_INFO("Waiting for image and faces, will publish face_features");
}


// Destructor
FaceFeaturesNode::~FaceFeaturesNode()
{}

// Image callback
void
FaceFeaturesNode::callback(const sensor_msgs::ImageConstPtr& image_msg,
                           const opencv_apps::FaceArrayStampedConstPtr& faces_msg)
{
    // convert image message to OpenCV
    cv_bridge::CvImagePtr cv_ptr;
    try {
        cv_ptr = cv_bridge::toCvCopy(image_msg, sensor_msgs::image_encodings::BGR8);
    } catch (cv_bridge::Exception& e) {
        ROS_ERROR("Failed to convert image message. Got cv_bridge exception: %s", e.what());
        return;
    }

    // make face detections vector
    std::vector<cv::Rect_<float> > face_detections;
    for (const auto &face_msg: faces_msg->faces)
    {
        cv::Rect_<float> rect;
        rect.x = face_msg.face.x;
        rect.y = face_msg.face.y;
        rect.width = face_msg.face.width;
        rect.height = face_msg.face.height;
        face_detections.push_back(rect);
    }

    // compute scale for processing
    float scale = 1.0;
    if (processing_image_width > 0) {
        scale = float(processing_image_width) / float(cv_ptr->image.cols);
        if (verbose) ROS_INFO_STREAM("Scaling image by " << scale);
    }


    // process image
    std::vector< FaceFeatureWrapper::FFResult > result_vec;
    if (!ff_wrapper.computeFaceFeatures(cv_ptr->image, face_detections,
            fx, fy, cx, cy, result_vec, scale, au_detection, visualizer_ptr))
    {
        ROS_ERROR("Failed to compute face features.");
        return;
    }
    if (result_vec.size() < 1) {
        if (verbose) {
            ROS_INFO_STREAM("computeFaceFeatures returned no results");
        }
        return;
    }
    if (verbose)
        ROS_INFO_STREAM("computeFaceFeatures found " << result_vec.size() << " faces.");

    // prepare output
    FaceFeaturesArray ffa_msg;
    ffa_msg.header = image_msg->header;

    int errors = 0;
    for (const auto& result: result_vec)
    {
        FaceFeatures ff_msg;
        ff_msg.success = result.success;

        if (ff_msg.success) {

            // store landmarks
            int num_pts = result.detected_landmarks.rows / 2;
            for (int i = 0; i < num_pts; i++){
                opencv_apps::Point2D point;
                point.x = result.detected_landmarks.at<float>(i,0) / scale;
                point.y = result.detected_landmarks.at<float>(i + num_pts,0) / scale;
                ff_msg.landmarks.push_back(point);

                int v = result.landmark_visibilities.at<int>(i);
                ff_msg.landmarks_visibilities.push_back(v);

                geometry_msgs::Vector3 point_3D;
                point_3D.x = result.detected_landmarks_3D.at<float>(0,i) / 1000.0; // convert from mm to meters
                point_3D.y = result.detected_landmarks_3D.at<float>(1,i) / 1000.0;
                point_3D.z = result.detected_landmarks_3D.at<float>(2,i) / 1000.0;
                ff_msg.landmarks_3D.push_back(point_3D);
            }

            // store gaze
            ff_msg.gaze_angle.x = result.gaze_angle[0];
            ff_msg.gaze_angle.y = result.gaze_angle[1];

            ff_msg.gaze_left.x = result.gaze_left.x;
            ff_msg.gaze_left.y = result.gaze_left.y;
            ff_msg.gaze_left.z = result.gaze_left.z;

            ff_msg.gaze_right.x = result.gaze_right.x;
            ff_msg.gaze_right.y = result.gaze_right.y;
            ff_msg.gaze_right.z = result.gaze_right.z;

            // store pose
            ff_msg.head_pose.position.x = result.pose_estimate[0] / 1000.0; // convert from mm to meters
            ff_msg.head_pose.position.y = result.pose_estimate[1] / 1000.0;
            ff_msg.head_pose.position.z = result.pose_estimate[2] / 1000.0;

            float Rx = result.pose_estimate[3]; // pitch
            float Ry = result.pose_estimate[4]; // yaw
            float Rz = result.pose_estimate[5]; // roll

            tf::Matrix3x3 matrix;
            euler2RotationMatrix(Rx, Ry, Rz, matrix);

            tf::Quaternion q;
            matrix.getRotation(q);

            ff_msg.head_pose.orientation.x = q.x();
            ff_msg.head_pose.orientation.y = q.y();
            ff_msg.head_pose.orientation.z = q.z();
            ff_msg.head_pose.orientation.w = q.w();

            // store aus
            if (au_detection) {
                for (const std::pair<std::string, double> &au_pair : result.current_au) {
                    ff_msg.au_name.push_back(au_pair.first);
                    ff_msg.au_intensity.push_back(au_pair.second);
                }
            }

        } else {
            errors++;
        }
        if (errors  > 0) {
            ROS_ERROR_STREAM("Error processing " << errors << " of " << result_vec.size() << " faces.");
        }

        ffa_msg.features.push_back(ff_msg);
    }

    // publish message
    if (ffa_msg.features.size() > 0) {
      pub_face_features.publish(ffa_msg);
    }

    if (verbose) {
      ROS_INFO_STREAM("Sent " << ffa_msg.features.size() << " face feature message for " << faces_msg->faces.size() << " input faces.");
    }

}

// main
int main(int argc, char **argv) {
    ros::init(argc, argv, "face_features_node", ros::init_options::AnonymousName);
    FaceFeaturesNode node = FaceFeaturesNode();
    ros::spin();
}
