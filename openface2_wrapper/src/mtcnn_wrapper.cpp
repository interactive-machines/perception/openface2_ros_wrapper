// Wrapper for the OpenFace 2.0 MTCNN face detector
// Created by Marynel Vazquez on 7/7/19.

#include "openface2_wrapper/mtcnn_wrapper.h"

#include <ros/package.h>

#include <cmath>
#include <stdexcept>

using namespace openface2_wrapper;

MTCNNWrapper::MTCNNWrapper()
{
    std::string pkg_path = ros::package::getPath("openface2_wrapper");
    mtcnn_face_detector_location = pkg_path + "/libraries/OpenFace/lib/local/LandmarkDetector/model/mtcnn_detector/MTCNN_detector.txt";
}

MTCNNWrapper::MTCNNWrapper(const std::string& face_detector_location)
{
    mtcnn_face_detector_location = face_detector_location;
}

MTCNNWrapper::~MTCNNWrapper()
{}

bool
MTCNNWrapper::loadFaceDetector()
{
    LandmarkDetector::FaceDetectorMTCNN face_detector_mtcnn(mtcnn_face_detector_location);
    if (face_detector_mtcnn.empty()){
        return false;
    }
    face_detector_mtcnn_ptr = std::make_shared< LandmarkDetector::FaceDetectorMTCNN >(face_detector_mtcnn);
    return true;
}

bool
MTCNNWrapper::detectFaces(const cv::Mat& input_image,
                          std::vector<cv::Rect_<float> >& face_detections,
                          std::vector<float>& confidences,
                          float scale)
{
    if (!face_detector_mtcnn_ptr || face_detector_mtcnn_ptr->empty()) return false;
    if (scale < 0) throw std::runtime_error("Scale cannot be negative in detect_faces().");

    bool resize_image = fabs(scale - 1.0) > 1e-5;

    cv::Mat rgb;
    if (resize_image) cv::resize(input_image, rgb, cv::Size(), scale, scale);
    else rgb = input_image;

    // first detect faces
    face_detections.clear();
    confidences.clear();
    LandmarkDetector::DetectFacesMTCNN(face_detections, rgb, *face_detector_mtcnn_ptr, confidences);

    // scale the detections back to the original size
    if (resize_image) {
        for (size_t i = 0; i < face_detections.size(); ++i) {
            face_detections.at(i).x = face_detections.at(i).x / scale;
            face_detections.at(i).y = face_detections.at(i).y / scale;
            face_detections.at(i).width = face_detections.at(i).width / scale;
            face_detections.at(i).height = face_detections.at(i).height / scale;
        }
    }

    return true;
}
