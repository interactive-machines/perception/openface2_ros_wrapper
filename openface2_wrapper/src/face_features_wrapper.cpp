// Wrapper for facial features processing
// Created by marynelv on 7/7/19.
//

#include "openface2_wrapper/face_features_node.h"
#include <openface2_wrapper/FaceFeatures.h>
#include <openface2_wrapper/FaceFeaturesArray.h>

#include <OpenFace/VisualizationUtils.h>
#include <OpenFace/GazeEstimation.h>

#include <ros/package.h>

#include <opencv2/imgproc/imgproc.hpp>

#include <cmath>
#include <sstream>
#include <stdexcept>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

using namespace openface2_wrapper;


// Constructor
FaceFeatureWrapper::FaceFeatureWrapper()
{
    std::string pkg_path = ros::package::getPath("openface2_wrapper");
    det_parameters.model_location = pkg_path + "/libraries/OpenFace/lib/local/LandmarkDetector/model/main_ceclm_general.txt";
    au_model = pkg_path + "/libraries/OpenFace/lib/local/FaceAnalyser/AU_predictors/main_static_svms.txt";

    init();
}

// Constructor
FaceFeatureWrapper::FaceFeatureWrapper(const std::string& landmarks_model, const std::string& au_model)
{
    det_parameters.model_location = landmarks_model;
    this->au_model = au_model;

    init();
}

// Destructor
FaceFeatureWrapper::~FaceFeatureWrapper()
{}

// Init FaceModelParams, FaceAnalyserParams, and visualizer
void
FaceFeatureWrapper::init()
{
    defaultInitFaceModelParams();
    defaultInitFaceAnalyserParams();
}

// Init FaceModelParams
void
FaceFeatureWrapper::defaultInitFaceModelParams()
{
    boost::filesystem::path model_path = boost::filesystem::path(det_parameters.model_location);
    std::string model_name = model_path.stem().string();
    if (model_name.compare("main_ceclm_general") == 0){
        det_parameters.curr_landmark_detector = LandmarkDetector::FaceModelParameters::CECLM_DETECTOR;
        det_parameters.sigma = 1.5f * det_parameters.sigma;
        det_parameters.reg_factor = 0.9f * det_parameters.reg_factor;
    } else if (model_name.compare("main_clnf_general") == 0){
        det_parameters.curr_landmark_detector = LandmarkDetector::FaceModelParameters::CLNF_DETECTOR;
    } else if (model_name.compare("main_clm_general") == 0){
        det_parameters.curr_landmark_detector = LandmarkDetector::FaceModelParameters::CLM_DETECTOR;
    } else {
        throw std::runtime_error("Invalid landmarks model. Should be one of: main_ceclm_general, main_clnf_general, main_clm_general.");
    }

    // set default parameters for landmark detection in the wild
    // taken from https://github.com/marynelv/OpenFace/blob/master/lib/local/LandmarkDetector/src/LandmarkDetectorParameters.cpp
    det_parameters.window_sizes_init = vector<int>(4);
    det_parameters.window_sizes_init[0] = 15;
    det_parameters.window_sizes_init[1] = 13;
    det_parameters.window_sizes_init[2] = 11;
    det_parameters.window_sizes_init[3] = 11;
    det_parameters.multi_view = true;
}

// Init FaceAnalyserParams
void
FaceFeatureWrapper::defaultInitFaceAnalyserParams()
{
    fa_parameters.OptimizeForImages();
}

// Set landmarks model
bool
FaceFeatureWrapper::loadLandmarksModel()
{
    // load landmarks model
    LandmarkDetector::CLNF face_model(det_parameters.model_location);
    if (!face_model.loaded_successfully)
    {
        return false;
    }
    face_model_ptr = std::make_shared< LandmarkDetector::CLNF >(face_model);
    return true;
}

// Set AU model
bool
FaceFeatureWrapper::loadAUModel()
{
    // save au model path
    fa_parameters.setModelLoc(au_model);
    // create face analyser
    face_analyser_ptr = FaceAnalyserPtr(new FaceAnalysis::FaceAnalyser(fa_parameters));
    // check if everything went ok
    if (face_analyser_ptr->GetAUClassNames().size() == 0) return false;
    return true;
}


// Compute features in an image
bool
FaceFeatureWrapper::computeFaceFeatures(const cv::Mat& input_image,
                                        const std::vector<cv::Rect_<float> >& face_detections,
                                        float fx, float fy, float cx, float cy,
                                        std::vector< FFResult >& result_vec,
                                        float scale, bool au_detection,
                                        VisualizerPtr visualizer_ptr)
{
    if (!face_model_ptr) return false;
    if (scale < 0) throw std::runtime_error("Scale cannot be negative in computeFeatures().");

    bool resize_image = fabs(scale - 1.0) > 1e-5;

    cv::Mat rgb;
    if (resize_image) {
        cv::resize(input_image, rgb, cv::Size(), scale, scale);
        fx = fx * scale;
        fy = fy * scale;
        cx = cx * scale;
        cy = cy * scale;
    } else {
        rgb = input_image;
    }

    if (visualizer_ptr) {
        visualizer_ptr->SetImage(rgb, fx, fy, cx, cy);
    }

    cv::Mat gray;
    cv::cvtColor(rgb, gray, cv::COLOR_RGB2GRAY);

    result_vec.clear();

    // detect landmarks
    for(const auto &const_rect: face_detections) {
        // convert faces to rect
        cv::Rect_<float> rect;
        rect.x = (scale > 0.0 ? const_rect.x * scale : const_rect.x);
        rect.y = (scale > 0.0 ? const_rect.y * scale : const_rect.y);
        rect.width = (scale > 0.0 ? const_rect.width * scale : const_rect.width);
        rect.height = (scale > 0.0 ? const_rect.height * scale : const_rect.height);

        // find landmarks
        FFResult result;
        result.success = LandmarkDetector::DetectLandmarksInImage(rgb, rect, *face_model_ptr,
                                                                  det_parameters, gray);

        // compute other features
        if (result.success) {

            // Get 3D landmarks
            result.detected_landmarks_3D = face_model_ptr->GetShape(fx, fy, cx, cy);

            // Store landmarks
            result.detected_landmarks = face_model_ptr->detected_landmarks;
            result.landmark_visibilities = face_model_ptr->GetVisibilities();

            // Visualize landmarks
            if (visualizer_ptr)
                visualizer_ptr->SetObservationLandmarks(result.detected_landmarks,
                                                        1.0, result.landmark_visibilities);


            // Estimate head pose. The format returned is [Tx, Ty, Tz, Eul_x, Eul_y, Eul_z]
            result.pose_estimate =
                    LandmarkDetector::GetPose(*face_model_ptr, fx, fy, cx, cy);

            // Gaze tracking, absolute gaze direction
            if (face_model_ptr->eye_model) {
                result.gaze_left = cv::Point3f(0, 0,-1.0);
                result.gaze_right = cv::Point3f(0, 0,-1.0);
                GazeAnalysis::EstimateGaze(*face_model_ptr, result.gaze_left, fx, fy, cx, cy, true);
                GazeAnalysis::EstimateGaze(*face_model_ptr, result.gaze_right, fx, fy, cx, cy, false);
                result.gaze_angle = GazeAnalysis::GetGazeAngle(result.gaze_left, result.gaze_right);
            }

            // Visualize pose & gaze
            if (visualizer_ptr) {
                visualizer_ptr->SetObservationPose(result.pose_estimate, 1.0);
                visualizer_ptr->SetObservationGaze(result.gaze_left, result.gaze_right,
                                                   LandmarkDetector::CalculateAllEyeLandmarks(*face_model_ptr),
                                                   LandmarkDetector::Calculate3DEyeLandmarks(*face_model_ptr, fx, fy, cx, cy),
                                                   face_model_ptr->detection_certainty);
            }

            // AU processing
            cv::Mat sim_warped_img;
            cv::Mat_<double> hog_descriptor;
            int num_hog_rows = 0, num_hog_cols = 0;

            if (au_detection) {
                face_analyser_ptr->PredictStaticAUsAndComputeFeatures(rgb, face_model_ptr->detected_landmarks);
                face_analyser_ptr->GetLatestAlignedFace(sim_warped_img);
                face_analyser_ptr->GetLatestHOG(hog_descriptor, num_hog_rows, num_hog_cols);

                result.current_au = face_analyser_ptr->GetCurrentAUsReg();

                if (visualizer_ptr)
                    visualizer_ptr->SetObservationActionUnits(
                            result.current_au,                         // regression (intensity)
                            face_analyser_ptr->GetCurrentAUsClass());  // classification (binary on/off)

            }

            // Scale landmarks
            if (resize_image)
                result.detected_landmarks = result.detected_landmarks / scale;
        }

        result_vec.push_back(result);

    }

    if (visualizer_ptr)
        visualizer_ptr->ShowObservation();

    return true;
}


