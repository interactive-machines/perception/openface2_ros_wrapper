OpenFace 2.0 ROS Wrapper
========================

ROS wrapper for [OpenFace 2.0](https://github.com/TadasBaltrusaitis/OpenFace). The code localy installs [OpenCV 3.4.0](https://github.com/opencv/opencv/tree/3.4.0) and [DLib 19.13](https://github.com/davisking/dlib/tree/v19.13) so that OpenFace can be compiled against them. No system installs of these libraries are required, thus preventing compatibility issues with ROS' OpenCV or any other version of DLib.

## Requirements

The code has been tested in Ubuntu 18.04 with [ROS Melodic](http://wiki.ros.org/melodic).

OpenFace and its dependencies require the following system libraries:

```bash
$ sudo apt update
$ sudo apt install build-essential cmake libopenblas-dev \
  libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev \
  libswscale-dev python-dev python-numpy libtbb2 libtbb-dev \
  libjpeg-dev libpng-dev libtiff-dev libdc1394-22-dev
```

## Installation Guide

1. Clone this repository (and OpenCV, DLib, and OpenFace submodules) into your catking workspace. Be patient. The OpenFace repository is huge. 

   ```bash
   $ git clone --recursive -j8 git@gitlab.com:interactive-machines/perception/openface2_ros_wrapper.git
   $ cd openface2_ros_wrapper
   ```

2. Get trained models provided by OpenFace.

   ```bash
   $ cd openface2_wrapper/libraries/OpenFace
   $ source download_models.sh
   $ cd ../../../
   ```

3. Set a local install directory for OpenCV, DLib, and OpenFace. 

   ```bash
   $ cd openface2_wrapper/libraries
   $ mkdir local_install
   $ export LOCAL_INSTALL=`pwd`/local_install
   $ cd ../..
   ```

4. Build OpenCV into the local install folder. Set as many threads as you can with the "-j<threads>" option when running `make` (e.g., use as many threads as twice the number of cores in your CPU if no other significant process is running in your machine). Compiling OpenCV takes a while...

   ```bash
   $ cd openface2_wrapper/libraries/opencv
   $ mkdir build; cd build
   $ cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=${LOCAL_INSTALL} -D BUILD_TIFF=ON -D WITH_TBB=ON -D BUILD_SHARED_LIBS=OFF -D BUILD_opencv_cudacodec=OFF ..
   $ make -j 12
   $ make install
   $ cd ../../../../
   ```

5. Build DLib into the local install folder. As before, tune "-j<threads>" based on your hardware.

   ```bash
   $ cd openface2_wrapper/libraries/dlib
   $ mkdir build; cd build
   $ cmake -D CMAKE_INSTALL_PREFIX=${LOCAL_INSTALL} -D DLIB_USE_CUDA=OFF ..
   $ cmake --build . --config Release -- -j 12
   $ make install
   $ cd ../../../../
   ```

6. Build OpenFace

   ```bash
   $ cd openface2_wrapper/libraries/OpenFace
   $ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${LOCAL_INSTALL}/lib
   $ export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:${LOCAL_INSTALL}/lib/pkgconfig
   $ export OpenCV_DIR=`pwd`/../opencv/build
   $ export dlib_DIR=${LOCAL_INSTALL}/lib/cmake/dlib
   $ mkdir build; cd build
   $ cmake -D CMAKE_BUILD_TYPE=RELEASE CMAKE_CXX_FLAGS="-std=c++11" -D CMAKE_EXE_LINKER_FLAGS="-std=c++11" -D CMAKE_INSTALL_PREFIX=${LOCAL_INSTALL} ..
   $ make -j 12
   $ make install
   $ cd ../../../../
   ```

   If everything went well, you should now be able to test OpenFace's samples. For example, the sample script below should process the input video "changeLighting.wmv" and display facial landmarks:

   ```bash
   $ cd openface2_wrapper/libraries/local_install/bin
   $ ./FaceLandmarkVid -f "../../OpenFace/samples/changeLighting.wmv"
   ```

7. Once OpenFace is built, you can compile the `openface2_wrapper` package with [catkin_make](http://wiki.ros.org/catkin/commands/catkin_make).

   ```bash
   $ roscd; cd .. # go to catkin workspace
   $ rosdep install -y -r --ignore-src --rosdistro=melodic --from-paths src # install dependencies 
   $ catkin_make -DCMAKE_BUILD_TYPE=Release # build
   ```


## Quick Start

Detect faces and face features:

```bash
$ roslaunch openface2_wrapper detect_faces_and_features.launch image_topic:=/camera/image_raw camera_info_topic:=/camera/camera_info
```

The faces should then be output to the `/faces` topic, and the features should be output to `/face_features`. 
See the [openface2_wrapper](openface2_wrapper) for more information.

## Acknowledgements

[OpenFace 2.0](https://github.com/TadasBaltrusaitis/OpenFace) is an implementation of a number of research papers from the Multicomp group, Language Technologies Institute at the Carnegie Mellon University and Rainbow Group, Computer Laboratory, University of Cambridge. The founder of the project and main developer is Tadas Baltrušaitis.